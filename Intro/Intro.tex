\section{Getting Started}
\label{sec:Intro} 

Welcome to Liquid Argon ATLAS shifts!
This manual should help you to complete all the tasks given to a LAr shifter. 

Should you find errors, things that are out-of-date, or sections that are confusing, please let us know. 
One of the jobs of the LAr shifters is to help make shifts better by informing us how we can improve the documentation.

Please put your comments/questions/additions on the
\htmladdnormallinkfoot {LAr JIRA project} {https://its.cern.ch/jira/browse/ATLASLAR/}.
where they can be seen and addressed by experts. 
If you have any questions, you can contact the LAr Super Shifters:
Emma Kuwertz and Adriana Milic.

Also see the \htmladdnormallinkfoot {ATLAS Glossary} {https://twiki.cern.ch/twiki/bin/view/Atlas/AtlasGlossary} for some help with all of the acronyms!
More information on OTP and class 1 shifts can be found \htmladdnormallinkfoot{here}{https://twiki.cern.ch/twiki/bin/viewauth/Atlas/OtpAtlasControlRoomShifts}.

\subsection{Before you arrive for shifts}

There are several things you can do before you arrive for shifts.

\begin{enumerate}

\item Get access to the Control Room. 
  You will need a CERN ID, with ``ATL\_CR'' access to get into the door of the main control room. 
  In order to get access rights you will need to have taken the following online courses: 
  \begin{itemize}
  \item \htmladdnormallinkfoot{Level 4A (ATLAS)} {https://sir.cern.ch/sir/f?p=106:55:103666944698775::NO::COURSE\_CURRENT\_ID:24}
  \item \htmladdnormallinkfoot{CERN Safety Introduction} {https://sir.cern.ch/sir/f?p=106:55:103666944698775::NO::COURSE\_CURRENT\_ID:403}
  \end{itemize}
  After completing the safety courses you must request ATL\_CR access in \htmladdnormallinkfoot{EDH}{https://edh.cern.ch/Document/General/ACRQ?zone=ATL\_CR}. 
  You can check your access rights and courses on \htmladdnormallinkfoot{adams}{http://adams.web.cern.ch/adams/} \hLink{https://hrt.cern.ch/hrt/AccessInfo}.
  Be sure to check on your access requests at least 2 weeks before your (shadow) shifts are due to begin!
  More details can be found at \hLink{https://atlasop.cern.ch/twiki/bin/view/Main/LArShiftSignup}.

%\item Get access the LAr satellite control room, next to the main control room. 
%  To do this, you have to go with your CERN
%  ID to S. Auerbach (located at 124-R011). Tell him you will take LAr
%  shifts and request access to 3159-R012. He will place your ID into
%  a machine and add the access rights there.
%\editnote{ESK: Removing part about requesting LAr satellite access - don't think this is necessary for shifters?}

\item You will need accounts for the Point 1 machines and e-log. 
  This is created automatically by the SysAdmins one day after you have been registered in OTP.
  The password for your P1 account will be the same as your
  NICE password for other application at CERN.
\item You are required to attend the general ATLAS shifter training sessions plus the dedicated Calo shifter training sessions {\bf in person}. 
  Links to all upcoming shifter training sessions can be found at \hLink{https://indico.cern.ch/event/359341/}.

You should read the most recent slides under ``General/LUCID/LAr/Tile Training'' linked from the \htmladdnormallinkfoot {calo portal}{https://atlasop.cern.ch/calo/}.
%``Shifter Tutorials \& Signup''
%found on the \htmladdnormallinkfoot {Shifter Operation Manual twiki page}
%{https://atlasop.cern.ch/twiki/bin/view/Main/CaloDetOperationManualShifter}.
This manual should be used as a general reference.

\end{enumerate}

\subsection{When you first arrive for shift}

You must arrive in the control room 15 minutes before your shift is due to start. 
You should use this time to liase with the previous calo shifter, 
making sure that you are up to date with any issues, problems or general observations from the previous shift and any other relevant information.

At the start of your shift:
\begin{itemize}
\item You MUST logout and log back in to the calo desk computers. 
  The username is ``crcal'' and you can just hit ``Enter'' in the password field (there is no password). 
  When prompted to select a role, choose ``CAL:shifter''.
\item The shifter environment will load automatically. 
  If you don't see the CAL menu on the bottom-left of the screen, you are not login as CAL:shifter. 
  Logout and log back in, selecting the correct role. 
\item  The start of shift checklist will open on the left most screen - the checklist is not mandatory, 
  but can help to guide you through the actions you should be taking at the start of your shift (but please do {\bf not} post start of shift elogs!).
  These checklists can be accessed anytime from the CAL menu on the bottom left of the screen (``CAL'' $\rightarrow$ ``CALChecklists''). 
  In addition to the ``Start of Shift'' checklist, there are the following: ``Shift tutorial'', ``Start of Run'' and ``End of Run'' checklists.
\item Familiarise yourself with the environment, making sure all the tools that you expect to see are open.
\item Pay attention to the ``Plan of the day,'' which will be posted on one of the wall-screens in the control room.
  As an ACR shifter you should be aware of what is going on (running status, beam conditions, etc).
  Don't be afraid to ask the experts questions! The ATLAS plan of the day can also be found on the \htmladdnormallinkfoot {calo portal}{https://atlasop.cern.ch/calo/} 
  under ``ATLAS Daily Program''.
\item Look at the \htmladdnormallinkfoot {Calo/FWD current status page}{https://atlasop.cern.ch/twiki/bin/view/Main/CurrentStatusCaloFwd}, 
  which is linked from the top of the calo portal. 
  Be sure to scroll down - this page contains known ERS messages and DCS warnings/alarms as well as up to date information on the status of the Calo/FWD systems. 
  This page should always be open so that you can easily check any errors/warnings that appear during your shift against those listed in the tables. 
  The errors/warnings you see may be ``known'' - in which case you'll find advice as to how to react. 
\item Check the Calorimeter Daily Program on the calo portal. Make sure you understand the plan of the day. Note that if you are on a morning shift that this program 
  may be updated later in the morning (e.g. after the daily run meeting), so be sure to check!
\item Read the ``Critical Notes for Shifters'' section of the calo portal. This is updated with important messages for shifters.
  %Here you can also find the ATLAS Daily Schedule and shifter notices. It is important that you read and understand these notices, the page is updated daily. 
  %The Whiteboard is a useful resource. It includes the phone numbers of relevant experts, and links to information about ``common problems'' and ``known issues.''
\end{itemize}
Note that the calo portal is a central resource for Calo/FWD shifters, with links to other useful twikis, tools and shifter training documentation. 
Be sure to have it open at all times.

\subsection{During the shift}

\begin{itemize}
\item Note the text editor open on the left most monitor. 
  It is recommended that you fill out the shift summary e-log template throughout your shift. 
  You should be familiar with it so that you know what details to add and when. 
  Details of each run during your shift should be recorded, in addition to any problems, 
  observations and encounters with experts (use the shift summary to keep a running commentary of your shift).
  Remember to save it regularly - accidental deletion causes massive frustration and loss of information!
  More information on elog usage and etiquette can be found in section~\ref{sec:elog}. 
\item When a new run is starting look at the DQMD monitoring.
  During the first few minutes of a run, it is ESSENTIAL to spot immediately data integrity problems which
  may require a stop and restart of the run.
\item Be familiar with the calibration plans for the day and know how to follow them through. 
  Sometimes the time window for calibration can become available suddenly and be quite tight. 
  You should therefore be able to react quickly. 
  Always make sure that the subsystem for which you plan to take calibrations for is not in use by the ATLAS partition before beginning a calibration run! 
  More details on calibration runs can be found in section~\ref{sec:calib}.
\item Monitor the detector status. The Detector Control Screen (DCS) and Final State Machine (FSM) are important monitoring tools.
  More information on these systems and what to look out for can be found in section~\ref{sec:DCS}.
  Specific issues/problems seen while on shift should be reported in a dedicated e-log (e.g. LAr HV trips need individual e-log entry).
  Check for known issues on the Current Status twiki page - a separate elog is not necessary in all cases. Don't be afraid to call the relevant experts. 
  Many common issues are also documented on the \htmladdnormallinkfoot {LAr}{https://atlasop.cern.ch/twiki/bin/view/Main/LArTroubleShooting\#ERRORS\_and\_WARNINGS} troubleshooting pages.
  Especially during the daylight hours an expert would prefer to receive a phonecall, even if an e-log is also posted, to be as up-to-date with events as possible.
\item Monitor Trigger and Data Acquisiation status (TDAQ).
  This includes using the DAQ Graphical User Interface, Error Reporting Service (ERS) and the Busy Panel. 
  To help diagnose problems the Trigger Rate Presenter and L1 Calo Map are useful resources. 
  Make sure you know how to navigate these tools quickly in the event of a problem.
  For an overall summary of the calorimeter DAQ status, the Calo DAQ Status tool is useful. 
  This should ideally be all green during a run, any entries turning to red should be followed up immediately.
\item Monitor the data integrity/quality status in using the Data Quality Monitoring Display (DQMD) and Online Histogram Presenter (OHP).
\item You will need to respond to problems / issues as they arise. Being up to date with the current calo status and daily plans will help you to act quickly.  
  More information on reacting to probelsms is provided in this section.
\end{itemize}

The rest of this manual should provide the information necessary to
deal with tasks and problems during the shift as they come up. Let us know should you find something lacking. \\

When problems come up, the procedure should be:

\begin{enumerate}
\item Know the information on the calo portal. 
  You should read it when you first arrive for shift, and keep it in mind. 
  It may contain instructions that are new or especially vital for this particular shift.
\item Look at the \htmladdnormallinkfoot{Current Status}{https://atlasop.cern.ch/twiki/bin/view/Main/CurrentStatusCaloFwd} 
  twiki to see whether the issue in question appears on there. 
  If it does you should find instructions as to your course of action.
\item Look at ``Guidelines for LAr errors'' on the \htmladdnormallinkfoot{LAr Troubleshooting twiki}{https://atlasop.cern.ch/twiki/bin/view/Main/LArTroubleShooting}, 
  which contains procedures for dealing errors on the fly, such as the LAr being ``busy'', TDAQ error messages, monitoring PT's crashing, OHP not showing plots, etc. 
  It helps to be familiar with this page, so read over it often so that you are aware of the procedures documented there.
%  You should also have checked the ``Expected alarms/error messages'' on the \htmladdnormallinkfoot{Calo Whiteboard}{https://atlasop.cern.ch/twiki/bin/view/Main/CaloDetWhiteBoard}.
\item If you cannot find the answer to your question on the Current Status page or the troubleshooting pages, 
  call the LAr Run Coordinator (70136) and please make a note of the fact that you couldn't find the documentation you needed in your e-log entry.
  For LAr errors you should ALWAYS call the LAr RC (70136). 
  They will liase with the relevant experts. 
  Please make a note in the shift summary e-log of any problem, even if the LAr RC says that it is a known problem (you can note that in the shift summary too).
\item In general, during daylight hours, you should never hesitate to call an expert if you are at all unsure of something. 
  Even if you think a warning is nothing it doesn't hurt to call and check. 
  Get in the habit of posting elogs describing and errors/warnings that you see in ERS and DCS if they are not documented/known (if a warning is ``known'' and harmless there is no need to post an elog), 
  and also link to these in your shift summary. 
  Remember to reload the elog page during your shift as an expert may have responded to one of your elogs in the meantime.
\end{enumerate}


\subsection{End of shift}

At the end of the shift, you will need to finish your e-log Shift Summary,
and submit it 15 MINUTES BEFORE the end of your shift.  
It should be completed by the time the next shifter arrives so that you can go through it with them.
Choose Message Type : Shift Summary; ShiftSummary\_Desk : Calo/Fwd,
System affected : ALFA (RPO),  LArg,  Lucid,  Tile,  ZDC; Status : closed, Subject : ``Shift summary for Calo/Fwd Desk''.




