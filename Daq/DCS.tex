
\subsection{DCS - Detector Control and Safety}\label{sec:DCS} 

To open the DCS Panel in the control room, go to the LAr menu on the bottom of the screen. Click : LAr
$\rightarrow$ DCS $\rightarrow$ LAr DCS FSM . Make sure you open this from the LAr button, not the DCS button, as the settings of the program will be different. \\ \\ From outside the control room, log on \verb&pcatlgcslin& through the Atlas gateway (with the lardaq account for example - see \ref{app:P1_Net}) and type: \verb&>/scratch/pvss/bin/viewlarfsm&
\\ \\ 
You should see the following graphic:
\\ \\ 
\begin{figure}[hb]
%\includegraphics{manual_pics/DCSPanelLArAll.png}
%\centerline{\epsfig{file=Daq/dcs1.eps,height=10cm}}
\includegraphics[height=10cm]{Daq/dcs1}
\caption{DCS FSM screen for LAr.}
\label{fig:DCS-LAr}
\end{figure}


\subsubsection{Check the LVPS Status}
\label{subs:DCS_LVPS}

To check the subsystems, go through each of the six detector parts: EMB A,
EMB C, EMEC A, EMEC C, HEC FCAL A, and HEC FCAL C. For each part, you
will get a global picture first (\ref{fig:DCS-LAr-compact} for
example) and then you can click further down the tree to see each set of crates (LV, HV, ROD).

\begin{figure}[hb]
\includegraphics[height=10cm]{Daq/dcs_lar_emba}
%\includegraphics[0,0][792,641]{Daq/dcs_lar_emba_smaller.png}
%\centerline{\epsfig{file=Daq/dcs_lar_emba.eps,height=10cm}}
%\centerline{\epsfig{file=Daq/dcs2.eps,height=10cm}}
\caption{Compact view of DCS for a half barrel. From inner circle to outer one, are displayed : ROD crates, Front End Crates (FECs), cooling loops, High Voltage (HV) for Power Supplies, High Voltage.}
\label{fig:DCS-LAr-compact}
\end{figure}

Starting with the EMB A as an example, first click ``EMB A''. 

If all five sub-sub-systems on the left-hand side say \verb&READY& and \verb&OK&,
and everything in the global picture is green, you can go on to the next
subsystem. This means the FEC LVPS, HV, and ROD crates are all on, or any problems below are known and do not propagate upwards. The run coordinators can mask known problems so LAr may still say ``OK'' even when some pieces are off.s

If anything in the global picture is gray, yellow, or red, or if there are any \verb&FATAL& or \verb&ERROR& conditions in the left-hand panel, you should follow this procedure:

\begin{enumerate}
\item Click to see the FEC status (for EMB A, click EMBA FEC). For each set of FEC's, a panel similar to the one shown in Figure~\ref{fig:DCS-LAr-LVPS} will pop up.
\begin{figure}[hb]
\centerline{\epsfig{file=Daq/DCS-LAr-LVPS.eps,height=10cm}}
\caption{DCS FSM screen for the LAr low voltage power supplies (LVPS).}
\label{fig:DCS-LAr-LVPS}
\end{figure}

\item Note the components which are off or have errors.

\item Check the \htmladdnormallinkfoot {LAr WhiteBoard} {http://pc-atlas-www.cern.ch/twiki/bin/view/Main/LArWhiteBoard} \htmladdnormallinkfoot {(*)}{https://atlasop.cern.ch/twiki/bin/view/Main/LArWhiteBoard} to see if the
components which are off or in error are already known problems. If
they are known, you do not need to notify an expert.

\item If the off/error components are NOT on the WhiteBoard, notify the LAr Run Coordinator by phone.
\end{enumerate}

Check the LVPS for the rest of the subsystems by clicking ``LAR'' at
the top of the tree to return you to the main page. Repeat the
procedure with the EMB C (EMBC FEC), EMEC A (EMEC A FEC), EMEC C (EMEC
C FEC), HEC FCAL A (HEC\_A\_LV), and HEC FCAL C (HEC\_C\_LV).

If you want to know more about one crate you can click on the crate
itself. (For the Barrel and EMEC, this will work, not for the HEC.) A
new panel will show the actual voltage on the O.C.E.M. power supply in
USA15 (around 280V), the current and the voltages on the output of the
LVPS (DC-DC converter) on the detector. 

\subsubsection{Check the ROD Crate Status}

To check the ReadOut Driver (ROD) crates, go through each of the six detector parts: EMB A,
EMB C, EMEC A, EMEC C, HEC FCAL A, and HEC FCAL C. For each part, you
will get a global picture first and then you can click further down the tree to see each set of crates.

\begin{enumerate}

\item Starting with the EMB A as an example, first click ``EMB A''. 

\item Click to see the ROD status (for EMB A, click ``EMB A ROD''). A panel similar to the one shown in Figure~\ref{fig:DCS-LAr-ROD}
should pop up.
\begin{figure}[hb]
\centerline{\epsfig{file=Daq/DCS-LAr-ROD.eps,height=10cm}}
\caption{DCS FSM screen for the LAr ROD crates in USA15.}
\label{fig:DCS-LAr-ROD}
\end{figure}

\item All the crates should say \verb&ON& and \verb&OK&. Note the
components which are off or have errors.

\item Check the \htmladdnormallinkfoot {LAr WhiteBoard} {http://pc-atlas-www.cern.ch/twiki/bin/view/Main/LArWhiteBoard} \htmladdnormallinkfoot {(*)}{https://atlasop.cern.ch/twiki/bin/view/Main/LArWhiteBoard} to see if the
components which are off or in error are already known problems. If
they are known, you do not need to notify an expert.

\item If the off/error components are NOT on the WhiteBoard, notify the LAr Run Coordinator by phone.
\end{enumerate}

Check the RODs for the rest of the subsystems by clicking ``LAR'' at
the top of the tree to return you to the main page. Repeat the
procedure with the EMB C, EMEC A, EMEC C, HEC FCAL A, and HEC FCAL C.

\subsubsection{Check HV Status}

To check the High Voltage (HV) status, go through each of the six detector parts: EMB A,
EMB C, EMEC A, EMEC C, HEC FCAL A, and HEC FCAL C. For each part, you
will get a global picture first and then you can click further down the tree to see each set of crates.

\begin{enumerate}

\item Starting with the EMB A as an example, first click ``EMB A''. 

\item Click to see the HV status (for EMB A, click ``EMB A HV''). A
panel similar to the one shown in Figure~\ref{fig:DCS-LAr-HV} should
pop up.

\begin{figure}[hb]
\centerline{\epsfig{file=Daq/DCS-LAr-HV.eps,width=15cm}}
\caption{DCS FSM screen for the barrel HV.}
\label{fig:DCS-LAr-HV}
\end{figure}

\item All the crates should say \verb&ON& and \verb&OK&. Note the
components which are off or have errors. 

% fixed? should check -- KC 22 july
%of the checklist. BE CAREFUL! Read the labels from the list of HV's on
%the left, NOT from the picture. The picture is sometimes mislabelled.

\item Check the \htmladdnormallinkfoot {LAr WhiteBoard} {http://pc-atlas-www.cern.ch/twiki/bin/view/Main/LArWhiteBoard} \htmladdnormallinkfoot {(*)}{https://atlasop.cern.ch/twiki/bin/view/Main/LArWhiteBoard} to see if the
components which are off or in error are already known problems. If they are known, you do
not need to notify an expert.

\item If the off/error components are NOT on the WhiteBoard,
double-check to make sure you are reading the PHI numbers from the
list and not the graphic, which is sometimes wrong. If it is really a
new problem, notify the LAr Run Coordinator.
\end{enumerate}

For the HV, there are many sub-subsystems to check. For the EMB, you
need to look at both ``EMB HV A'' and ``LAR\_EMBPSA\_HV'' for the
PreSampler. The same goes for EMB C. The EMEC A and EMEC C have only
one HV to check, while HEC FCAL A has two again (HEC A HV \& FCAL A
HV), and the same for HEC FCAL C (HEC C HV \& FCAL C HV).

\subsubsection{Check the DCS Alarms Screen}

To open the DCS Alarm Screen, go to the \emph{LAr menu} (NOT the DCS
menu) on the bottom of the screen. Click : LAr $\rightarrow$ DCS
$\rightarrow$ LAr DCS Alarms. This will show all of the DCS alarms
that apply to the LAr systems -- temperature, voltages, etc.

Alarms have a color code, and a letter in the first column. 

\begin{itemize}
\item FATAL - F - red
\item ERROR - E - orange
\item WARNING - W - yellow
\end{itemize}

If you see any red/(F)/FATAL errors, check the WhiteBoard first and
then call the Run Coordinator.

For each other alarm, right-click and select ``Trend''. The orange
line is the level for an ERROR, the yellow line is for WARNING. The
blue line is the quantity as measured.

Next, put the mouse cursor on the x-axis (time), and then use the
scroll wheel to zoom out. You can see if the value is constant over
the last few hours/days or if it is rapidly changing.

For any WARNINGs or ERRORs that are not rapidly changing, check the
WhiteBoard to see if they are known. If they are not known, make an
entry in the e-log with the ``complete'' alarm information (the full line). 
They will be evaluated by an expert, and if
the value is OK, the thresholds can be changed later.

Keep this screen open and visible throughout the shift, reacting as
above for each new alarm.

* If you see ALL the alarms for ATLAS, you opened DCS $\rightarrow$ DCS Alarms, not LAr $\rightarrow$ DCS $\rightarrow$ LAr DCS
Alarms.

* If there are over 1000 alarms, maybe the HECLV is still in the filter.

Look for new HECLV problems (within the past day), and check for them
on the WhiteBoard or with an expert. Once you have dealt with the HEC
problems, you want to filter out all the old messages. 

Double-click in the white box on the bottom left underneath the
checkbox ``Systems''. Click on ATLLARCLVTEMP, then hold down control
and click on the other ATLLAR* systems. Do NOT include
ATLLARHECLV. Next click the button that says ``Apply filter'' on the
bottom right. Most of the messages should be gone.






